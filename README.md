# TruckDev

### Acesse nosso guia do [Notion](https://www.notion.so/TruckDev-c8d1acd2d6264c159c9027970b56dc2b)

#### Caso não queira seguir o passo a passo do guia do Notion, execute os scripts abaixo para instalar as tecnologias necessárias que você irá utilizar e configurar seu ambiente virtual com o virtualenv :)

### Scripts para configuração de ambiente:

 #### :whale2: Docker:

```bash
bash docker.sh -y
```
 #### :cloud: Google Cloud:

```bash
bash google_cloud.sh -y
```
 #### :anchor: Kubernetes:

```bash
bash kubernetes.sh -y
```

 #### :computer: virtualenv 
 Após a execução do script, você deverá executar alguns passos de configuração na sua IDE, veja no [Notion](https://www.notion.so/virtualenv-6017209c55f54fd4925357babd69e1d1), a partir do item 4.

```bash
bash virtualenv.sh -y
```