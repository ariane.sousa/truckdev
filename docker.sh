## Atualizando pacotes
echo "Atualizando pacotes"
sudo apt update
sleep 3
echo ""
echo "Pré-Configurando o ambiente"
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
sleep 3
echo ""
echo "Baixando o Docker"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sleep 3
echo ""
echo "Adicionando aos repositorios"
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sleep 3
echo ""
echo "Atualizando pacotes"
sudo apt update
sleep 3
echo ""
echo ""
apt-cache policy docker-ce
sleep 3
echo ""
echo "Instalando o Docker"
sudo apt install docker-ce -y
sleep 3
echo ""
echo "Verificando instalação do Docker"
sudo docker version
sleep 3

## [Opicional, porem ajuda] - Configurando Docker sem o Sudo

echo ""
echo "Configurando Docker sem o Sudo"
sudo usermod -aG docker ${USER}
sleep 3

# Docker Composer
echo ""
echo "Baixando script do Docker Compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sleep 3
echo
echo "Dando as permissões do Docker Compose"
sudo chmod +x /usr/local/bin/docker-compose
sleep 3
echo ""
echo "Verificando instalação do Docker Compose"
docker-compose --version

