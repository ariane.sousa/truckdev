echo "-------------------------------------"
echo "*** Download the latest release ***"
echo "-------------------------------------"
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
sleep 3
echo "-------------------------------------"
echo "*** kubectl binary executable ***"
echo "-------------------------------------"
chmod +x ./kubectl
sleep 3
echo "-------------------------------------"
echo "** Move the binary in to your PATH **"
echo "-------------------------------------"
sudo mv ./kubectl /usr/local/bin/kubectl
sleep 3
echo "-----------------------------------------------------------"
echo "** Test to ensure the version installed is up-to-date: **"
echo "-----------------------------------------------------------"
kubectl version --client
sleep 3
echo "-------------------------------------"
echo "** GET PODS (HOMOLOG) **"
echo "-------------------------------------"
kubectl get pods -n homolog
