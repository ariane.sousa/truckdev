echo "|------------------------|"
echo "| ATUALIZANDO PACOTES... |"
echo "|------------------------|"
echo ""
sleep 3
sudo apt update
echo "Add the Cloud SDK distribution URI as a package source"
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
sleep 3
echo "Import the Google Cloud Platform public key"
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
sleep 3
echo "Update the package list and install the Cloud SDK"
Update the package list and install the Cloud SDK
sleep 3
sudo apt-get update --fix-missing
sleep 5
sudo apt-get update && sudo apt-get install google-cloud-sdk
sleep 3
echo "Initialize the SDK:"
gcloud init
sleep 3
gcloud container clusters get-credentials cluster-development --region us-east1
