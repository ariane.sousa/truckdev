echo "---------------------------------"
echo "*** Instalando o virtualenv ***"
echo "---------------------------------"
sudo pip install virtualenv
sleep 3
echo "---------------------------------"
echo "Criando o ambiente virtual......."
echo "---------------------------------"
virtualenv venv --python=python3
sleep 3
echo "---------------------------------"
echo "Ativando o ambiente virtual......."
echo "---------------------------------"
source venv/bin/activate

